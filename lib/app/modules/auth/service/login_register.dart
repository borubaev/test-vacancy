import 'package:firebase_auth/firebase_auth.dart';

class LoginService {
  static Future<UserCredential?> register(String email, String password) async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    try {
      final user = await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
    } catch (e) {
      print('Register error: ${e.toString()}');
      return null;
    }
  }

  static Future<UserCredential?> login(String email, String password) async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    try {
      final user = await _auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      return user;
    } catch (e) {
      print('Register error: ${e.toString()}');
      return null;
    }
  }
}
