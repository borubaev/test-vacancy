import 'package:flash_chat/app/components/animation_logo.dart';
import 'package:flash_chat/app/components/custom_button.dart';
import 'package:flash_chat/app/components/custom_input.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/auth_controller.dart';

class AuthView extends GetView<AuthController> {
  const AuthView(this.isLogin, {Key? key}) : super(key: key);
  final bool isLogin;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Form(
                key: controller.formkey,
                child: Column(
                  children: [
                    const AnimationLogo(
                      size: 120,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    CustomInput(
                      controller: controller.email,
                      text: 'Enter your email or number',
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    CustomInput(
                      controller: controller.password,
                      text: 'Enter your password',
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              CustomButton(
                text: isLogin ? 'Login' : 'Register',
                color: Colors.blue,
                onpressed: isLogin
                    ? () async => await controller.registerLogin(true)
                    : () async => await controller.registerLogin(false),
              )
            ],
          ),
        ),
      ),
    );
  }
}
